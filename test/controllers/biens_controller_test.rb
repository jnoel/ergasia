require 'test_helper'

class BiensControllerTest < ActionController::TestCase
  setup do
    @bien = biens(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:biens)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bien" do
    assert_difference('Bien.count') do
      post :create, bien: { adresse: @bien.adresse, cp: @bien.cp, locataire: @bien.locataire, nom: @bien.nom, note: @bien.note, proprietaire: @bien.proprietaire, surf_hab: @bien.surf_hab, surf_terr: @bien.surf_terr, type: @bien.type, ville: @bien.ville }
    end

    assert_redirected_to bien_path(assigns(:bien))
  end

  test "should show bien" do
    get :show, id: @bien
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bien
    assert_response :success
  end

  test "should update bien" do
    patch :update, id: @bien, bien: { adresse: @bien.adresse, cp: @bien.cp, locataire: @bien.locataire, nom: @bien.nom, note: @bien.note, proprietaire: @bien.proprietaire, surf_hab: @bien.surf_hab, surf_terr: @bien.surf_terr, type: @bien.type, ville: @bien.ville }
    assert_redirected_to bien_path(assigns(:bien))
  end

  test "should destroy bien" do
    assert_difference('Bien.count', -1) do
      delete :destroy, id: @bien
    end

    assert_redirected_to biens_path
  end
end
