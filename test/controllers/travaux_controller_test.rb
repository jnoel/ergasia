require 'test_helper'

class TravauxControllerTest < ActionController::TestCase
  setup do
    @travail = travaux(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:travaux)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create travail" do
    assert_difference('Travail.count') do
      post :create, travail: { bien_id: @travail.bien_id, detail: @travail.detail, travaux_statut_id: @travail.travaux_statut_id }
    end

    assert_redirected_to travail_path(assigns(:travail))
  end

  test "should show travail" do
    get :show, id: @travail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @travail
    assert_response :success
  end

  test "should update travail" do
    patch :update, id: @travail, travail: { bien_id: @travail.bien_id, detail: @travail.detail, travaux_statut_id: @travail.travaux_statut_id }
    assert_redirected_to travail_path(assigns(:travail))
  end

  test "should destroy travail" do
    assert_difference('Travail.count', -1) do
      delete :destroy, id: @travail
    end

    assert_redirected_to travaux_path
  end
end
