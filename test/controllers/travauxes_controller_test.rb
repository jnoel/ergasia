require 'test_helper'

class TravauxesControllerTest < ActionController::TestCase
  setup do
    @travaux = travauxes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:travauxes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create travaux" do
    assert_difference('Travaux.count') do
      post :create, travaux: { bien_id: @travaux.bien_id, detail: @travaux.detail, travaux_statut_id: @travaux.travaux_statut_id }
    end

    assert_redirected_to travaux_path(assigns(:travaux))
  end

  test "should show travaux" do
    get :show, id: @travaux
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @travaux
    assert_response :success
  end

  test "should update travaux" do
    patch :update, id: @travaux, travaux: { bien_id: @travaux.bien_id, detail: @travaux.detail, travaux_statut_id: @travaux.travaux_statut_id }
    assert_redirected_to travaux_path(assigns(:travaux))
  end

  test "should destroy travaux" do
    assert_difference('Travaux.count', -1) do
      delete :destroy, id: @travaux
    end

    assert_redirected_to travauxes_path
  end
end
