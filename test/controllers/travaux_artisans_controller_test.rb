require 'test_helper'

class TravauxArtisansControllerTest < ActionController::TestCase
  setup do
    @travaux_artisan = travaux_artisans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:travaux_artisans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create travaux_artisan" do
    assert_difference('TravauxArtisan.count') do
      post :create, travaux_artisan: { tier_id: @travaux_artisan.tier_id, travail_id: @travaux_artisan.travail_id }
    end

    assert_redirected_to travaux_artisan_path(assigns(:travaux_artisan))
  end

  test "should show travaux_artisan" do
    get :show, id: @travaux_artisan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @travaux_artisan
    assert_response :success
  end

  test "should update travaux_artisan" do
    patch :update, id: @travaux_artisan, travaux_artisan: { tier_id: @travaux_artisan.tier_id, travail_id: @travaux_artisan.travail_id }
    assert_redirected_to travaux_artisan_path(assigns(:travaux_artisan))
  end

  test "should destroy travaux_artisan" do
    assert_difference('TravauxArtisan.count', -1) do
      delete :destroy, id: @travaux_artisan
    end

    assert_redirected_to travaux_artisans_path
  end
end
