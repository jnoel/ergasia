require 'test_helper'

class TravauxStatutsControllerTest < ActionController::TestCase
  setup do
    @travaux_statut = travaux_statuts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:travaux_statuts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create travaux_statut" do
    assert_difference('TravauxStatut.count') do
      post :create, travaux_statut: { nom: @travaux_statut.nom, ordre: @travaux_statut.ordre }
    end

    assert_redirected_to travaux_statut_path(assigns(:travaux_statut))
  end

  test "should show travaux_statut" do
    get :show, id: @travaux_statut
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @travaux_statut
    assert_response :success
  end

  test "should update travaux_statut" do
    patch :update, id: @travaux_statut, travaux_statut: { nom: @travaux_statut.nom, ordre: @travaux_statut.ordre }
    assert_redirected_to travaux_statut_path(assigns(:travaux_statut))
  end

  test "should destroy travaux_statut" do
    assert_difference('TravauxStatut.count', -1) do
      delete :destroy, id: @travaux_statut
    end

    assert_redirected_to travaux_statuts_path
  end
end
