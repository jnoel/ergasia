require 'test_helper'

class TravauxTypesControllerTest < ActionController::TestCase
  setup do
    @travaux_type = travaux_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:travaux_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create travaux_type" do
    assert_difference('TravauxType.count') do
      post :create, travaux_type: { nom: @travaux_type.nom }
    end

    assert_redirected_to travaux_type_path(assigns(:travaux_type))
  end

  test "should show travaux_type" do
    get :show, id: @travaux_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @travaux_type
    assert_response :success
  end

  test "should update travaux_type" do
    patch :update, id: @travaux_type, travaux_type: { nom: @travaux_type.nom }
    assert_redirected_to travaux_type_path(assigns(:travaux_type))
  end

  test "should destroy travaux_type" do
    assert_difference('TravauxType.count', -1) do
      delete :destroy, id: @travaux_type
    end

    assert_redirected_to travaux_types_path
  end
end
