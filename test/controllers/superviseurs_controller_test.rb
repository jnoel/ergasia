require 'test_helper'

class SuperviseursControllerTest < ActionController::TestCase
  setup do
    @superviseur = superviseurs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:superviseurs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create superviseur" do
    assert_difference('Superviseur.count') do
      post :create, superviseur: { travail_id: @superviseur.travail_id, user_id: @superviseur.user_id }
    end

    assert_redirected_to superviseur_path(assigns(:superviseur))
  end

  test "should show superviseur" do
    get :show, id: @superviseur
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @superviseur
    assert_response :success
  end

  test "should update superviseur" do
    patch :update, id: @superviseur, superviseur: { travail_id: @superviseur.travail_id, user_id: @superviseur.user_id }
    assert_redirected_to superviseur_path(assigns(:superviseur))
  end

  test "should destroy superviseur" do
    assert_difference('Superviseur.count', -1) do
      delete :destroy, id: @superviseur
    end

    assert_redirected_to superviseurs_path
  end
end
