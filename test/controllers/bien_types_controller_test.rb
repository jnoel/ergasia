require 'test_helper'

class BienTypesControllerTest < ActionController::TestCase
  setup do
    @bien_type = bien_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bien_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bien_type" do
    assert_difference('BienType.count') do
      post :create, bien_type: { nom: @bien_type.nom }
    end

    assert_redirected_to bien_type_path(assigns(:bien_type))
  end

  test "should show bien_type" do
    get :show, id: @bien_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bien_type
    assert_response :success
  end

  test "should update bien_type" do
    patch :update, id: @bien_type, bien_type: { nom: @bien_type.nom }
    assert_redirected_to bien_type_path(assigns(:bien_type))
  end

  test "should destroy bien_type" do
    assert_difference('BienType.count', -1) do
      delete :destroy, id: @bien_type
    end

    assert_redirected_to bien_types_path
  end
end
