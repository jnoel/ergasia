require 'test_helper'

class TiersTypesControllerTest < ActionController::TestCase
  setup do
    @tiers_type = tiers_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tiers_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tiers_type" do
    assert_difference('TiersType.count') do
      post :create, tiers_type: { nom: @tiers_type.nom }
    end

    assert_redirected_to tiers_type_path(assigns(:tiers_type))
  end

  test "should show tiers_type" do
    get :show, id: @tiers_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tiers_type
    assert_response :success
  end

  test "should update tiers_type" do
    patch :update, id: @tiers_type, tiers_type: { nom: @tiers_type.nom }
    assert_redirected_to tiers_type_path(assigns(:tiers_type))
  end

  test "should destroy tiers_type" do
    assert_difference('TiersType.count', -1) do
      delete :destroy, id: @tiers_type
    end

    assert_redirected_to tiers_types_path
  end
end
