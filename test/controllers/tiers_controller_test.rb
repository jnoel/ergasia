require 'test_helper'

class TiersControllerTest < ActionController::TestCase
  setup do
    @tier = tiers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tiers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tier" do
    assert_difference('Tier.count') do
      post :create, tier: { adresse: @tier.adresse, cp: @tier.cp, email: @tier.email, fax: @tier.fax, is_locataire: @tier.is_locataire, is_prestataire: @tier.is_prestataire, is_proprietaire: @tier.is_proprietaire, nom: @tier.nom, portable: @tier.portable, prenom: @tier.prenom, tel: @tier.tel, ville: @tier.ville }
    end

    assert_redirected_to tier_path(assigns(:tier))
  end

  test "should show tier" do
    get :show, id: @tier
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tier
    assert_response :success
  end

  test "should update tier" do
    patch :update, id: @tier, tier: { adresse: @tier.adresse, cp: @tier.cp, email: @tier.email, fax: @tier.fax, is_locataire: @tier.is_locataire, is_prestataire: @tier.is_prestataire, is_proprietaire: @tier.is_proprietaire, nom: @tier.nom, portable: @tier.portable, prenom: @tier.prenom, tel: @tier.tel, ville: @tier.ville }
    assert_redirected_to tier_path(assigns(:tier))
  end

  test "should destroy tier" do
    assert_difference('Tier.count', -1) do
      delete :destroy, id: @tier
    end

    assert_redirected_to tiers_path
  end
end
