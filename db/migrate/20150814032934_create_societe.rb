class CreateSociete < ActiveRecord::Migration
  def change
    create_table :societes do |t|
      t.string :nom, null: false
      t.text :adresse
      t.string :cp
      t.string :ville
      t.string :tel
      t.string :fax
      t.string :mail
      t.attachment :logo
      t.timestamps null: false
    end
    Societe.create nom: "Société Test"
  end
end
