class AddTypeToImportation < ActiveRecord::Migration
  def change
    add_column :importations, :type, :string
  end
end
