class AddStatutsToTravauxStatut < ActiveRecord::Migration
  def change
    TravauxStatut.create nom: "Demande devis", ordre: 1.5 unless TravauxStatut.where(nom: "Demande devis").first
    TravauxStatut.create nom: "Info bailleur", ordre: 2.2 unless TiersType.where(nom: "Info bailleur").first
    TravauxStatut.create nom: "Retour bailleur", ordre: 2.5 unless TiersType.where(nom: "Retour bailleur").first
    TravauxStatut.create nom: "Bon pour paiement", ordre: 5.5 unless TiersType.where(nom: "Facture reçue").first
  end
end
