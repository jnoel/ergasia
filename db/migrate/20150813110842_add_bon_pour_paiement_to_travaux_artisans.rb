class AddBonPourPaiementToTravauxArtisans < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :bon_pour_paiement, :boolean
  end
end
