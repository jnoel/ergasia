class AddSuperviseurToUsers < ActiveRecord::Migration
  def change
    add_column :users, :superviseur, :boolean, :default => false
  end
end
