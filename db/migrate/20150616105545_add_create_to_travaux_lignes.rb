class AddCreateToTravauxLignes < ActiveRecord::Migration
  def change
    add_column :travaux_lignes, :created_at, :datetime
    add_column :travaux_lignes, :updated_at, :datetime
  end
end
