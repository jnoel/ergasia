class RemoveDevisFromTravauxArtisans < ActiveRecord::Migration
  def change
    remove_column :travaux_artisans, :devis, :boolean
    add_attachment :travaux_artisans, :devis
  end
end
