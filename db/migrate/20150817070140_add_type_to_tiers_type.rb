class AddTypeToTiersType < ActiveRecord::Migration
  def change
    TiersType.create nom: "Locataire" unless TiersType.where(nom: "Locataire").first
    TiersType.create nom: "Propriétaire" unless TiersType.where(nom: "Propriétaire").first
    TiersType.create nom: "Artisan" unless TiersType.where(nom: "Artisan").first
    TiersType.create nom: "Syndic" unless TiersType.where(nom: "Syndic").first
  end
end
