class RemoveFinTravauxFromTravauxArtisan < ActiveRecord::Migration
  def change
    remove_column :travaux_artisans, :fin_travaux, :boolean
    add_attachment :travaux_artisans, :fin_travaux
  end
end
