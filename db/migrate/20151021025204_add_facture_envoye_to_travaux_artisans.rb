class AddFactureEnvoyeToTravauxArtisans < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :facture_envoye, :boolean, default: false
  end
end
