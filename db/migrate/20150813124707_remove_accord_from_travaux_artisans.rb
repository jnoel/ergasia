class RemoveAccordFromTravauxArtisans < ActiveRecord::Migration
  def change
    remove_column :travaux_artisans, :accord, :boolean
    remove_column :travaux_artisans, :facture, :boolean
    add_attachment :travaux_artisans, :accord
    add_attachment :travaux_artisans, :facture
  end
end
