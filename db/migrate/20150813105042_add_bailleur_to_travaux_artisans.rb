class AddBailleurToTravauxArtisans < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :bailleur, :boolean
  end
end
