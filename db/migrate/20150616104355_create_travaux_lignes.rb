class CreateTravauxLignes < ActiveRecord::Migration
  def change
    create_table :travaux_lignes do |t|
      t.text :detail
      t.datetime :date
    end
  end
end
