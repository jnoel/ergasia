class AddBoolToTravauxArtisan < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :demande, :boolean
    add_column :travaux_artisans, :fin_travaux, :boolean
    add_column :travaux_artisans, :paye, :boolean
  end
end
