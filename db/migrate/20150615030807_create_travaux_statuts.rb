class CreateTravauxStatuts < ActiveRecord::Migration
  def change
    create_table :travaux_statuts do |t|
      t.string :nom
      t.float :ordre

      t.timestamps null: false
    end
  end
end
