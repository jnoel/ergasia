class RemoveDateFromTravauxLignes < ActiveRecord::Migration
  def change
    remove_column :travaux_lignes, :date, :datetime
  end
end
