class CreateTravauxArtisans < ActiveRecord::Migration
  def change
    create_table :travaux_artisans do |t|
      t.integer :travail_id
      t.integer :tier_id

      t.timestamps null: false
    end
  end
end
