class AddDefaultUserToUser < ActiveRecord::Migration
  def self.up
    user = User.create!( :login => "admin", :name => "Admin", :admin => true, :email => 'admin@admin.com', :timezone => "Abu Dhabi", :password => 'admin', :password_confirmation => 'admin', :job => "Administrateur")
  end

  def self.down
    user = User.find_by_login( 'admin' )
    user.destroy
  end
end
