class AddEnvoiMailToSociete < ActiveRecord::Migration
  def change
    add_column :societes, :envoi_mail, :boolean
  end
end
