class AddUserToTravauxLignes < ActiveRecord::Migration
  def change
  	change_table :travaux_lignes do |t|
      t.attachment :bpa
      t.attachment :facture
      t.integer :user_id
      t.boolean :demande, :default => false
      t.boolean :payer, :default => false
    end
  end
end
