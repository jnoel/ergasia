class CreateSuperviseurs < ActiveRecord::Migration
  def change
    create_table :superviseurs do |t|
      t.integer :travail_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
