class AddMessageAutomatiqueToTravauxLigne < ActiveRecord::Migration
  def change
    add_column :travaux_lignes, :message_automatique, :boolean, default: false
  end
end
