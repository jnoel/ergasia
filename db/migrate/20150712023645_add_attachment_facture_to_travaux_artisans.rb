class AddAttachmentFactureToTravauxArtisans < ActiveRecord::Migration
  def self.up
    change_table :travaux_artisans do |t|
      t.attachment :facture
    end
  end

  def self.down
    remove_attachment :travaux_artisans, :facture
  end
end
