class AddFiDevisToTravauxArtisan < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :fi_devis, :boolean
    add_column :travaux_artisans, :fi_details, :text
  end
end
