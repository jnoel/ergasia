class AddJobToUsers < ActiveRecord::Migration
  def change
  	change_table :users do |t|
      t.attachment :avatar
      t.string :job
      t.boolean :admin, :default => false
    end
  end
end
