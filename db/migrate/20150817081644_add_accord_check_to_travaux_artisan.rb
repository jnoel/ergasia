class AddAccordCheckToTravauxArtisan < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :accord_check, :boolean
  end
end
