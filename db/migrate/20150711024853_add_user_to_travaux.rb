class AddUserToTravaux < ActiveRecord::Migration
  def change
  	change_table :travaux do |t|
      t.integer :user_id
    end
  end
end
