class AddBoolToTravauxArtisans < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :devis, :boolean
    add_column :travaux_artisans, :accord, :boolean
    add_column :travaux_artisans, :facture, :boolean
  end
end
