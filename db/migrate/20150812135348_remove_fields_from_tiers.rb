class RemoveFieldsFromTiers < ActiveRecord::Migration
  def change
    remove_column :tiers, :is_locataire, :boolean
    remove_column :tiers, :is_proprietaire, :boolean
    remove_column :tiers, :is_prestataire, :boolean
    change_column :tiers, :tiers_type_id, :integer, null: false
  end
end
