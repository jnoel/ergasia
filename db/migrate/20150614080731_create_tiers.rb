class CreateTiers < ActiveRecord::Migration
  def change
    create_table :tiers do |t|
      t.string :nom
      t.string :prenom
      t.text :adresse
      t.string :cp
      t.string :ville
      t.string :tel
      t.string :fax
      t.string :portable
      t.string :email
      t.boolean :is_locataire
      t.boolean :is_proprietaire
      t.boolean :is_prestataire

      t.timestamps null: false
    end
  end
end
