class AddDefaultTypeToBienType < ActiveRecord::Migration
  def change
    BienType.create nom: "Appartement" unless BienType.where(nom: "Appartement").first
    BienType.create nom: "Vila" unless BienType.where(nom: "Vila").first
  end
end
