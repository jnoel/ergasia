class AddDefaultTypeToTravauxType < ActiveRecord::Migration
  def change
    TravauxType.create nom: "Electricité" unless TravauxType.where(nom: "Electricité").first
    TravauxType.create nom: "Plomberie" unless TravauxType.where(nom: "Plomberie").first
  end
end
