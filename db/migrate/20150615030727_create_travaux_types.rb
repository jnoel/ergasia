class CreateTravauxTypes < ActiveRecord::Migration
  def change
    create_table :travaux_types do |t|
      t.string :nom

      t.timestamps null: false
    end
  end
end
