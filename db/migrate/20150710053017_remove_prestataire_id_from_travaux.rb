class RemovePrestataireIdFromTravaux < ActiveRecord::Migration
  def change
    remove_column :travaux, :prestataire_id, :integer
  end
end
