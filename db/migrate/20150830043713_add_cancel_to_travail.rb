class AddCancelToTravail < ActiveRecord::Migration
  def change
    add_column :travaux, :cancel, :boolean, :default => false
    add_column :travaux, :cancel_raison, :text
  end
end
