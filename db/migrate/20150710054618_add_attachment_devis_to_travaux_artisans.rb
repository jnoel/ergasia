class AddAttachmentDevisToTravauxArtisans < ActiveRecord::Migration
  def self.up
    change_table :travaux_artisans do |t|
      t.attachment :devis
    end
  end

  def self.down
    remove_attachment :travaux_artisans, :devis
  end
end
