class AddNotesToTier < ActiveRecord::Migration
  def change
    add_column :tiers, :notes, :text
  end
end
