class CreateTravaux < ActiveRecord::Migration
  def change
    create_table :travaux do |t|
      t.integer :bien_id
      t.integer :travaux_statut_id
      t.text :detail

      t.timestamps null: false
    end
  end
end
