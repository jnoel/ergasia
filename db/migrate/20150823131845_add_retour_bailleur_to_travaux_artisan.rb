class AddRetourBailleurToTravauxArtisan < ActiveRecord::Migration
  def change
    add_column :travaux_artisans, :accord_bailleur, :boolean
    add_column :travaux_artisans, :accord_bailleur_message, :text
    add_attachment :travaux_artisans, :accord_bailleur_fichier
  end
end
