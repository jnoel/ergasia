class AddAttachmentAccordToTravauxArtisans < ActiveRecord::Migration
  def self.up
    change_table :travaux_artisans do |t|
      t.attachment :accord
    end
  end

  def self.down
    remove_attachment :travaux_artisans, :accord
  end
end
