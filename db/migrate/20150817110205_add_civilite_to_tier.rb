class AddCiviliteToTier < ActiveRecord::Migration
  def change
    add_column :tiers, :civilite, :string
    add_column :tiers, :raison_sociale, :string
  end
end
