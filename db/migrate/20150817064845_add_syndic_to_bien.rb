class AddSyndicToBien < ActiveRecord::Migration
  def change
    add_column :biens, :syndic_id, :integer
    add_column :biens, :coproprietaire_id, :integer
    add_column :biens, :colocataire_id, :integer
  end
end
