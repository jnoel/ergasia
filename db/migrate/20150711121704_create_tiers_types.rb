class CreateTiersTypes < ActiveRecord::Migration
  def change
    create_table :tiers_types do |t|
      t.string :nom

      t.timestamps null: false
    end
  end
end
