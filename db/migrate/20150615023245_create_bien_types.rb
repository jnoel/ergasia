class CreateBienTypes < ActiveRecord::Migration
  def change
    create_table :bien_types do |t|
      t.string :nom

      t.timestamps null: false
    end
  end
end
