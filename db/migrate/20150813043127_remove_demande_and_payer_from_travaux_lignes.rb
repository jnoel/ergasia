class RemoveDemandeAndPayerFromTravauxLignes < ActiveRecord::Migration
  def change
    remove_column :travaux_lignes, :demande, :boolean
    remove_column :travaux_lignes, :payer, :boolean
  end
end
