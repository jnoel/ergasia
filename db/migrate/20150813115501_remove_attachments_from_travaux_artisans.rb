class RemoveAttachmentsFromTravauxArtisans < ActiveRecord::Migration
  def change
    remove_attachment :travaux_artisans, :devis
    remove_attachment :travaux_artisans, :accord
    remove_attachment :travaux_artisans, :facture
  end
end
