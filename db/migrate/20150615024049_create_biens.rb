class CreateBiens < ActiveRecord::Migration
  def change
    create_table :biens do |t|
      t.string :nom
      t.integer :bien_type_id
      t.integer :proprietaire_id
      t.integer :locataire_id
      t.text :adresse
      t.string :cp
      t.string :ville
      t.integer :surf_hab
      t.integer :surf_terr
      t.text :note

      t.timestamps null: false
    end
  end
end
