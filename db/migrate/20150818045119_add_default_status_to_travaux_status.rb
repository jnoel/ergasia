class AddDefaultStatusToTravauxStatus < ActiveRecord::Migration
  def change
    TravauxStatut.create nom: "Nouveau travail", ordre: 1.0 unless TravauxStatut.where(nom: "Nouveau travail").first
    TravauxStatut.create nom: "Devis reçu", ordre: 2.0 unless TiersType.where(nom: "Devis reçu").first
    TravauxStatut.create nom: "Bon pour accord", ordre: 3.0 unless TiersType.where(nom: "Bon pour accord").first
    TravauxStatut.create nom: "Fin des travaux", ordre: 4.0 unless TiersType.where(nom: "Fin des travaux").first
    TravauxStatut.create nom: "Facture reçue", ordre: 5.0 unless TiersType.where(nom: "Facture reçue").first
    TravauxStatut.create nom: "Facture payée", ordre: 6.0 unless TiersType.where(nom: "Facture payée").first
  end
end
