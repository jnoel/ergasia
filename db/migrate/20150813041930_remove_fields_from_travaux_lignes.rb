class RemoveFieldsFromTravauxLignes < ActiveRecord::Migration
  def change
    remove_attachment :travaux_lignes, :bpa
    remove_attachment :travaux_lignes, :facture
    add_attachment :travaux_lignes, :piece_jointe
  end
end
