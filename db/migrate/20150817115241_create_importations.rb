class CreateImportations < ActiveRecord::Migration
  def change
    create_table :importations do |t|
      t.integer :user_id, null: false
      t.string :titre, null: false
      t.text :notes
      t.attachment :fichier, null: false

      t.timestamps null: false
    end
  end
end
