class RemoveDetailFromTravaux < ActiveRecord::Migration
  def change
    remove_column :travaux, :detail, :text
  end
end
