# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151203103846) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bien_types", force: :cascade do |t|
    t.string   "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "biens", force: :cascade do |t|
    t.integer  "bien_type_id"
    t.integer  "proprietaire_id"
    t.integer  "locataire_id"
    t.text     "adresse"
    t.string   "cp"
    t.string   "ville"
    t.integer  "surf_hab"
    t.integer  "surf_terr"
    t.text     "note"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "ref"
    t.integer  "syndic_id"
    t.integer  "coproprietaire_id"
    t.integer  "colocataire_id"
  end

  create_table "importations", force: :cascade do |t|
    t.integer  "user_id",              null: false
    t.string   "titre",                null: false
    t.text     "notes"
    t.string   "fichier_file_name",    null: false
    t.string   "fichier_content_type", null: false
    t.integer  "fichier_file_size",    null: false
    t.datetime "fichier_updated_at",   null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "type"
  end

  create_table "messageries", force: :cascade do |t|
    t.integer "travaux_ligne_id"
    t.integer "user_id"
  end

  create_table "societes", force: :cascade do |t|
    t.string   "nom",               null: false
    t.text     "adresse"
    t.string   "cp"
    t.string   "ville"
    t.string   "tel"
    t.string   "fax"
    t.string   "mail"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.boolean  "envoi_mail"
  end

  create_table "superviseurs", force: :cascade do |t|
    t.integer  "travail_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tiers", force: :cascade do |t|
    t.string   "nom"
    t.string   "prenom"
    t.text     "adresse"
    t.string   "cp"
    t.string   "ville"
    t.string   "tel"
    t.string   "fax"
    t.string   "portable"
    t.string   "email"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "tiers_type_id",  null: false
    t.string   "civilite"
    t.string   "raison_sociale"
    t.text     "notes"
    t.string   "tel_bureau"
  end

  create_table "tiers_types", force: :cascade do |t|
    t.string   "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "travaux", force: :cascade do |t|
    t.integer  "bien_id"
    t.integer  "travaux_statut_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "travaux_type_id"
    t.integer  "user_id"
    t.boolean  "cancel",            default: false
    t.text     "cancel_raison"
  end

  create_table "travaux_artisans", force: :cascade do |t|
    t.integer  "travail_id"
    t.integer  "tier_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.boolean  "demande"
    t.boolean  "paye"
    t.boolean  "bailleur"
    t.boolean  "bon_pour_paiement"
    t.string   "devis_file_name"
    t.string   "devis_content_type"
    t.integer  "devis_file_size"
    t.datetime "devis_updated_at"
    t.string   "accord_file_name"
    t.string   "accord_content_type"
    t.integer  "accord_file_size"
    t.datetime "accord_updated_at"
    t.string   "facture_file_name"
    t.string   "facture_content_type"
    t.integer  "facture_file_size"
    t.datetime "facture_updated_at"
    t.boolean  "fi_devis"
    t.text     "fi_details"
    t.string   "fin_travaux_file_name"
    t.string   "fin_travaux_content_type"
    t.integer  "fin_travaux_file_size"
    t.datetime "fin_travaux_updated_at"
    t.boolean  "accord_check"
    t.boolean  "accord_bailleur"
    t.text     "accord_bailleur_message"
    t.string   "accord_bailleur_fichier_file_name"
    t.string   "accord_bailleur_fichier_content_type"
    t.integer  "accord_bailleur_fichier_file_size"
    t.datetime "accord_bailleur_fichier_updated_at"
    t.float    "fi_montant"
    t.boolean  "facture_envoye",                       default: false
    t.string   "type_paiement"
  end

  create_table "travaux_lignes", force: :cascade do |t|
    t.text     "detail"
    t.integer  "travail_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "message_automatique",       default: false
    t.string   "piece_jointe_file_name"
    t.string   "piece_jointe_content_type"
    t.integer  "piece_jointe_file_size"
    t.datetime "piece_jointe_updated_at"
  end

  create_table "travaux_statuts", force: :cascade do |t|
    t.string   "nom"
    t.float    "ordre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "travaux_types", force: :cascade do |t|
    t.string   "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.string   "name"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "timezone"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "job"
    t.boolean  "admin",               default: false
    t.string   "email"
    t.text     "signature"
    t.boolean  "superviseur",         default: false
    t.string   "tel"
  end

end
