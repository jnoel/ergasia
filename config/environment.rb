# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Load the app's custom environment variables here, before environments/*.rb
smtp_vars = File.join(Rails.root, 'config', 'initializers', 'smtp_vars.rb')
load(smtp_vars) if File.exists?(smtp_vars)

# Initialize the Rails application.
Rails.application.initialize!
