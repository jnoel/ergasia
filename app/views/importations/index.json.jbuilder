json.array!(@importations) do |importation|
  json.extract! importation, :id, :user_id, :titre, :notes, :fichier
  json.url importation_url(importation, format: :json)
end
