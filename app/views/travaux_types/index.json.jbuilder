json.array!(@travaux_types) do |travaux_type|
  json.extract! travaux_type, :id, :nom
  json.url travaux_type_url(travaux_type, format: :json)
end
