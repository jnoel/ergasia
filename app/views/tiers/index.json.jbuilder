json.array!(@tiers) do |tier|
  json.extract! tier, :id, :nom, :prenom, :adresse, :cp, :ville, :tel, :fax, :portable, :email, :is_locataire, :is_proprietaire, :is_prestataire
  json.url tier_url(tier, format: :json)
end
