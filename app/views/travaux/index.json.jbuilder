json.array!(@travaux) do |travail|
  json.extract! travail, :id, :bien_id, :travaux_statut_id, :detail
  json.url travail_url(travail, format: :json)
end
