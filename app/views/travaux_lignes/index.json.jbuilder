json.array!(@travaux_lignes) do |travaux_ligne|
  json.extract! travaux_ligne, :id, :detail
  json.url travaux_ligne_url(travaux_ligne, format: :json)
end
