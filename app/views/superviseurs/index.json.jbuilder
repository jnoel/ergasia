json.array!(@superviseurs) do |superviseur|
  json.extract! superviseur, :id, :travail_id, :user_id
  json.url superviseur_url(superviseur, format: :json)
end
