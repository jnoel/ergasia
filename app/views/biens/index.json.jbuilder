json.array!(@biens) do |bien|
  json.extract! bien, :id, :nom, :type, :proprietaire, :locataire, :adresse, :cp, :ville, :surf_hab, :surf_terr, :note
  json.url bien_url(bien, format: :json)
end
