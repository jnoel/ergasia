json.array!(@tiers_types) do |tiers_type|
  json.extract! tiers_type, :id, :nom
  json.url tiers_type_url(tiers_type, format: :json)
end
