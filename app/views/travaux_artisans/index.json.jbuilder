json.array!(@travaux_artisans) do |travaux_artisan|
  json.extract! travaux_artisan, :id, :travail_id, :tier_id
  json.url travaux_artisan_url(travaux_artisan, format: :json)
end
