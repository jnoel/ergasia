json.array!(@bien_types) do |bien_type|
  json.extract! bien_type, :id, :nom
  json.url bien_type_url(bien_type, format: :json)
end
