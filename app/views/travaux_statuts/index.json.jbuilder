json.array!(@travaux_statuts) do |travaux_statut|
  json.extract! travaux_statut, :id, :nom, :ordre
  json.url travaux_statut_url(travaux_statut, format: :json)
end
