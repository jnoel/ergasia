json.extract! @user, :id, :login, :name, :crypted_password, :password_salt, :persistence_token, :timezone, :created_at, :updated_at
