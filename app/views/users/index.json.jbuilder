json.array!(@users) do |user|
  json.extract! user, :id, :login, :name, :crypted_password, :password_salt, :persistence_token, :timezone
  json.url user_url(user, format: :json)
end
