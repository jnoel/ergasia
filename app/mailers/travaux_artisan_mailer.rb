class TravauxArtisanMailer < ApplicationMailer
  def mail_travaux_artisan(from, to, message, ligne, sujet, cc)
    @message = message
    attachments[ligne.piece_jointe_file_name] = File.read(ligne.piece_jointe.path)
    societe = Societe.first
    mail(to: to, from: from, subject: sujet, cc: cc) if societe.envoi_mail
  end
end
