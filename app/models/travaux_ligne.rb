class TravauxLigne < ActiveRecord::Base
	validates :detail,  :presence => true
	validates :user_id,  :presence => true
	validates :travail_id,  :presence => true
	belongs_to :travail
	belongs_to :user
	has_many :messagerie, :dependent => :delete_all
	has_attached_file :piece_jointe
	#validates_attachment :piece_jointe, content_type: { content_type: "application/pdf" }
	do_not_validate_attachment_file_type :piece_jointe

	after_create :envoi_sur_messagerie

	private
		def envoi_sur_messagerie
			superviseurs = Superviseur.where(travail_id: self.travail_id)
			superviseurs.each do |superviseur|
				Messagerie.create(travaux_ligne_id: self.id, user_id: superviseur.user_id) unless self.user_id==superviseur.user_id
			end
		end
end
