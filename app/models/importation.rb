class Importation < ActiveRecord::Base
  validates :titre,  :presence => true
  validates :user_id,  :presence => true
  validates :fichier,  :presence => true

  has_attached_file :fichier
  do_not_validate_attachment_file_type :fichier
  belongs_to :user
  self.inheritance_column = :_type_disabled
end
