class Travail < ActiveRecord::Base
	validates :bien_id,  :presence => true
	validates :travaux_type_id,  :presence => true
	validates :travaux_statut_id,  :presence => true
	validates :user_id,  :presence => true
	belongs_to :travaux_statut
	belongs_to :bien
	belongs_to :travaux_type
	belongs_to :tier, foreign_key: "prestataire_id"
	belongs_to :user
	has_many :travaux_lignes, :dependent => :delete_all
	has_many :travaux_artisans, :dependent => :delete_all
	has_many :superviseurs, :dependent => :delete_all
end
