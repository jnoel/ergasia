class User < ActiveRecord::Base
	acts_as_authentic do |c|
		c.crypto_provider = Authlogic::CryptoProviders::Sha512
  end
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  has_many :travaux_lignes
  has_many :travaux

end
