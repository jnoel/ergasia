class Tier < ActiveRecord::Base
	validates :tiers_type_id,  :presence => true
	belongs_to :tiers_type
	has_many :biens
	has_many :travaux

	def nom_complet
		return "#{self.raison_sociale} #{self.nom} #{self.prenom}"
	end

	def statut
		stat = []
		stat << "Propriétaire" if self.is_proprietaire
		stat << "Locataire" if self.is_locataire
		stat << "Prestataire" if self.is_prestataire
		return stat
	end
end
