class TravauxArtisan < ActiveRecord::Base
	belongs_to :travail
	belongs_to :tier
	has_attached_file :devis
	do_not_validate_attachment_file_type :devis
	has_attached_file :accord
	do_not_validate_attachment_file_type :accord
	has_attached_file :facture
	do_not_validate_attachment_file_type :facture
	has_attached_file :fin_travaux
	do_not_validate_attachment_file_type :fin_travaux
	has_attached_file :accord_bailleur_fichier
	do_not_validate_attachment_file_type :accord_bailleur_fichier
end
