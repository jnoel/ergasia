class Bien < ActiveRecord::Base

	validates :ville,  :presence => true

	belongs_to :proprietaire, :class_name => 'Tier', :foreign_key => 'proprietaire_id'
	belongs_to :locataire, :class_name => 'Tier', :foreign_key => 'locataire_id'
	belongs_to :coproprietaire, :class_name => 'Tier', :foreign_key => 'coproprietaire_id'
	belongs_to :colocataire, :class_name => 'Tier', :foreign_key => 'colocataire_id'
	belongs_to :syndic, :class_name => 'Tier', :foreign_key => 'syndic_id'

	belongs_to :bien_type
	has_many :travaux

	def nom
		return "#{self.bien_type.nom} sur #{self.ville}"
	end

	def nom_ref
		return "#{self.nom} (#{self.ref})"
	end

	def adresse_complete
		return "#{self.adresse}\n#{self.cp} #{self.ville}"
	end
end
