class TravauxLignesController < ApplicationController
  before_action :set_travaux_ligne, only: [:show, :edit, :update, :destroy]

  # GET /travaux_lignes
  # GET /travaux_lignes.json
  def index
    @travaux_lignes = TravauxLigne.all
  end

  # GET /travaux_lignes/1
  # GET /travaux_lignes/1.json
  def show
  end

  # GET /travaux_lignes/new
  def new
    @travaux_ligne = TravauxLigne.new
  end

  # GET /travaux_lignes/1/edit
  def edit
  end

  # POST /travaux_lignes
  # POST /travaux_lignes.json
  def create
    @travaux_ligne = TravauxLigne.new(travaux_ligne_params)
    @travaux_ligne.user_id = current_user.id
    respond_to do |format|
      if @travaux_ligne.save
        format.html { redirect_to travail_path(@travaux_ligne.travail_id), notice: 'Travaux ligne was successfully created.' }
        format.json { render :show, status: :created, location: @travaux_ligne }
      else
        format.html { render :new }
        format.json { render json: @travaux_ligne.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /travaux_lignes/1
  # PATCH/PUT /travaux_lignes/1.json
  def update
    respond_to do |format|
      if @travaux_ligne.update(travaux_ligne_params)
        format.html { redirect_to @travaux_ligne, notice: 'Travaux ligne was successfully updated.' }
        format.json { render :show, status: :ok, location: @travaux_ligne }
      else
        format.html { render :edit }
        format.json { render json: @travaux_ligne.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travaux_lignes/1
  # DELETE /travaux_lignes/1.json
  def destroy
    @travaux_ligne.destroy
    respond_to do |format|
      format.html { redirect_to travaux_lignes_url, notice: 'Travaux ligne was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travaux_ligne
      @travaux_ligne = TravauxLigne.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travaux_ligne_params
      params.require(:travaux_ligne).permit(:detail, :travail_id, :piece_jointe)
    end
end
