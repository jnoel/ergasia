class BiensController < ApplicationController
  before_action :set_bien, only: [:show, :edit, :update, :destroy]
  before_action :set_select, only: [:edit, :new, :create]

  # GET /biens
  # GET /biens.json
  def index
  	tiers_id = params["tiers"]
    @biens = tiers_id ? Bien.where("proprietaire_id=? OR locataire_id=? OR coproprietaire_id=? OR colocataire_id=?", tiers_id, tiers_id, tiers_id, tiers_id) : Bien.all
    @titre = tiers_id ? "BIENS - #{Tier.find(tiers_id).nom_complet}" : "BIENS"

  	gon.biens = []
  	@biens.each do |bien|
  		proprietaires = []
      proprietaires << bien.proprietaire.nom_complet if bien.proprietaire
      proprietaires << bien.coproprietaire.nom_complet if bien.coproprietaire
      locataires = []
      locataires << bien.locataire.nom_complet if bien.locataire
      locataires << bien.colocataire.nom_complet if bien.colocataire
      gon.biens << {id: bien.id,
  									ref: bien.ref,
  									type: bien.bien_type.nom,
  									ville: bien.ville,
  									proprietaire: proprietaires.join(", "),
  									locataire: locataires.join(", "),
  									surf_hab: bien.surf_hab.to_i,
  									surf_terr: bien.surf_terr.to_i
  								 }
  	end
  end

  # GET /biens/1
  # GET /biens/1.json
  def show
  	@travaux_count = Travail.where("bien_id=? AND cancel=false AND travaux_statut_id<6", @bien.id).count
  end

  # GET /biens/new
  def new
    @bien = Bien.new
  end

  # GET /biens/1/edit
  def edit
  end

  # POST /biens
  # POST /biens.json
  def create
    @bien = Bien.new(bien_params)

    respond_to do |format|
      if @bien.save
        format.html { redirect_to @bien, notice: 'Bien was successfully created.' }
        format.json { render :show, status: :created, location: @bien }
      else
        format.html { render :new }
        format.json { render json: @bien.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /biens/1
  # PATCH/PUT /biens/1.json
  def update
    respond_to do |format|
      if @bien.update(bien_params)
        format.html { redirect_to @bien, notice: 'Bien was successfully updated.' }
        format.json { render :show, status: :ok, location: @bien }
      else
        format.html { render :edit }
        format.json { render json: @bien.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /biens/1
  # DELETE /biens/1.json
  def destroy
    @bien.destroy
    respond_to do |format|
      format.html { redirect_to biens_url, notice: 'Bien was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bien
      @bien = Bien.find(params[:id])
    end

    def set_select
    	@types = BienType.all
    	@proprietaires = Tier.where(tiers_type_id: 2).order(:nom, :prenom)
    	@locataires = Tier.where(tiers_type_id: 1).order(:nom, :prenom)
      @syndics = Tier.where(tiers_type_id: 4).order(:nom, :prenom)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bien_params
      params.require(:bien).permit(:bien_type_id, :proprietaire_id, :locataire_id, :coproprietaire_id, :colocataire_id, :syndic_id, :adresse, :cp, :ville, :surf_hab, :surf_terr, :note, :ref)
    end
end
