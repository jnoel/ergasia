class TiersController < ApplicationController
  before_action :set_tier, only: [:show, :edit, :update, :destroy]

  # GET /tiers
  # GET /tiers.json
  def index
    tiers = Tier.all
    gon.tiers = []
  	tiers.each do |tier|
  		gon.tiers << {id: tier.id,
  									nom: tier.nom,
  									prenom: tier.prenom,
  									ville: tier.ville,
  									tel: tier.tel,
  									email: tier.email,
  									type: tier.tiers_type.nom,
                    raison_sociale: tier.raison_sociale
  								 }
  	end
  end

  # GET /tiers/1
  # GET /tiers/1.json
  def show
  	@maison_count = Bien.where("locataire_id=? OR proprietaire_id=? OR colocataire_id=? OR coproprietaire_id=?", @tier.id, @tier.id, @tier.id, @tier.id).count
  	@travaux_count = Travail.joins(:bien).where("(biens.locataire_id=? OR biens.proprietaire_id=? OR biens.colocataire_id=? OR biens.coproprietaire_id=?) AND cancel=false AND travaux_statut_id<6", @tier.id, @tier.id, @tier.id, @tier.id).count unless @tier.tiers_type_id==3
  	@travaux_count = TravauxArtisan.where(tier_id: @tier.id).count if @tier.tiers_type_id==3
  	@travaux_url = "/travaux?#{@tier.tiers_type_id==3 ? 'artisan' : 'tiers'}=#{@tier.id}"
    @factures_count = TravauxArtisan.where("tier_id=? AND facture_file_size>0", @tier.id).count if @tier.tiers_type_id==3
    @factures_url = "/factures?artisan=#{@tier.id}"
  end

  # GET /tiers/new
  def new
    @tier = Tier.new
  end

  # GET /tiers/1/edit
  def edit
  end

  # POST /tiers
  # POST /tiers.json
  def create
    @tier = Tier.new(tier_params)

    respond_to do |format|
      if @tier.save
        format.html { redirect_to @tier, notice: 'Tier was successfully created.' }
        format.json { render :show, status: :created, location: @tier }
      else
        format.html { render :new }
        format.json { render json: @tier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tiers/1
  # PATCH/PUT /tiers/1.json
  def update
    respond_to do |format|
      if @tier.update(tier_params)
        format.html { redirect_to @tier, notice: 'Tier was successfully updated.' }
        format.json { render :show, status: :ok, location: @tier }
      else
        format.html { render :edit }
        format.json { render json: @tier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tiers/1
  # DELETE /tiers/1.json
  def destroy
    @tier.destroy
    respond_to do |format|
      format.html { redirect_to tiers_url, notice: 'Tier was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tier
      @tier = Tier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tier_params
      params.require(:tier).permit(:nom, :prenom, :adresse, :cp, :ville, :tel, :fax, :portable, :email, :notes, :is_locataire, :is_proprietaire, :is_prestataire, :tiers_type_id, :civilite, :raison_sociale)
    end
end
