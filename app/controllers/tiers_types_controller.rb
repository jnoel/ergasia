class TiersTypesController < ApplicationController
  before_action :set_tiers_type, only: [:show, :edit, :update, :destroy]

  # GET /tiers_types
  # GET /tiers_types.json
  def index
    @tiers_types = TiersType.all
  end

  # GET /tiers_types/1
  # GET /tiers_types/1.json
  def show
  end

  # GET /tiers_types/new
  def new
    @tiers_type = TiersType.new
  end

  # GET /tiers_types/1/edit
  def edit
  end

  # POST /tiers_types
  # POST /tiers_types.json
  def create
    @tiers_type = TiersType.new(tiers_type_params)

    respond_to do |format|
      if @tiers_type.save
        format.html { redirect_to @tiers_type, notice: 'Tiers type was successfully created.' }
        format.json { render :show, status: :created, location: @tiers_type }
      else
        format.html { render :new }
        format.json { render json: @tiers_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tiers_types/1
  # PATCH/PUT /tiers_types/1.json
  def update
    respond_to do |format|
      if @tiers_type.update(tiers_type_params)
        format.html { redirect_to @tiers_type, notice: 'Tiers type was successfully updated.' }
        format.json { render :show, status: :ok, location: @tiers_type }
      else
        format.html { render :edit }
        format.json { render json: @tiers_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tiers_types/1
  # DELETE /tiers_types/1.json
  def destroy
    @tiers_type.destroy
    respond_to do |format|
      format.html { redirect_to tiers_types_url, notice: 'Tiers type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tiers_type
      @tiers_type = TiersType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tiers_type_params
      params.require(:tiers_type).permit(:nom)
    end
end
