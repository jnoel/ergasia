class SocietesController < ApplicationController
  before_action :set_societe

  # GET /societes/1/edit
  def edit
  end

  # PATCH/PUT /societes/1
  # PATCH/PUT /societes/1.json
  def update
    respond_to do |format|
      if @societe.update(societer_params)
        format.html { redirect_to "/config", notice: 'Societe was successfully updated.' }
        format.json { render :show, status: :ok, location: @societe }
      else
        format.html { render :edit }
        format.json { render json: @societe.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_societe
      @societe = Societe.find(1)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def societer_params
      params.require(:societe).permit(:nom, :adresse, :cp, :ville, :tel, :fax, :mail, :logo, :envoi_mail)
    end
end
