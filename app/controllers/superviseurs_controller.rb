class SuperviseursController < ApplicationController
  before_action :set_superviseur, only: [:show, :edit, :update, :destroy]

  # GET /superviseurs
  # GET /superviseurs.json
  def index
    @superviseurs = Superviseur.all
  end

  # GET /superviseurs/1
  # GET /superviseurs/1.json
  def show
  end

  # GET /superviseurs/new
  def new
    @superviseur = Superviseur.new
  end

  # GET /superviseurs/1/edit
  def edit
  end

  # POST /superviseurs
  # POST /superviseurs.json
  def create
    @superviseur = Superviseur.new(superviseur_params)
		superviseur_exist = Superviseur.where(travail_id: @superviseur.travail_id, user_id: @superviseur.user_id)
		if superviseur_exist.count > 0
			redirect_to @superviseur.travail
		else
		  respond_to do |format|
		    if @superviseur.save
		      format.html { redirect_to @superviseur.travail, notice: 'Superviseur was successfully created.' }
		      format.json { render :show, status: :created, location: @superviseur }
		    else
		      format.html { render :new }
		      format.json { render json: @superviseur.errors, status: :unprocessable_entity }
		    end
		  end
		end
  end

  # PATCH/PUT /superviseurs/1
  # PATCH/PUT /superviseurs/1.json
  def update
    respond_to do |format|
      if @superviseur.update(superviseur_params)
        format.html { redirect_to @superviseur, notice: 'Superviseur was successfully updated.' }
        format.json { render :show, status: :ok, location: @superviseur }
      else
        format.html { render :edit }
        format.json { render json: @superviseur.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /superviseurs/1
  # DELETE /superviseurs/1.json
  def destroy
    travail = @superviseur.travail
    @superviseur.destroy
    respond_to do |format|
      format.html { redirect_to travail, notice: 'Superviseur was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_superviseur
      @superviseur = Superviseur.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def superviseur_params
      params.require(:superviseur).permit(:travail_id, :user_id)
    end
end
