class HomeController < ApplicationController
  def index
  	@messages = current_user ? Messagerie.where(user_id: current_user.id) : []
  end
end
