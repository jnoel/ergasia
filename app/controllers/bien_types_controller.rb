class BienTypesController < ApplicationController
  before_action :set_bien_type, only: [:show, :edit, :update, :destroy]

  # GET /bien_types
  # GET /bien_types.json
  def index
    @bien_types = BienType.all
  end

  # GET /bien_types/1
  # GET /bien_types/1.json
  def show
  end

  # GET /bien_types/new
  def new
    @bien_type = BienType.new
  end

  # GET /bien_types/1/edit
  def edit
  end

  # POST /bien_types
  # POST /bien_types.json
  def create
    @bien_type = BienType.new(bien_type_params)

    respond_to do |format|
      if @bien_type.save
        format.html { redirect_to @bien_type, notice: 'Bien type was successfully created.' }
        format.json { render :show, status: :created, location: @bien_type }
      else
        format.html { render :new }
        format.json { render json: @bien_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bien_types/1
  # PATCH/PUT /bien_types/1.json
  def update
    respond_to do |format|
      if @bien_type.update(bien_type_params)
        format.html { redirect_to @bien_type, notice: 'Bien type was successfully updated.' }
        format.json { render :show, status: :ok, location: @bien_type }
      else
        format.html { render :edit }
        format.json { render json: @bien_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bien_types/1
  # DELETE /bien_types/1.json
  def destroy
    @bien_type.destroy
    respond_to do |format|
      format.html { redirect_to bien_types_url, notice: 'Bien type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bien_type
      @bien_type = BienType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bien_type_params
      params.require(:bien_type).permit(:nom)
    end
end
