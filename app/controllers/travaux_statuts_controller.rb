class TravauxStatutsController < ApplicationController
  before_action :set_travaux_statut, only: [:show, :edit, :update, :destroy]

  # GET /travaux_statuts
  # GET /travaux_statuts.json
  def index
    @travaux_statuts = TravauxStatut.order(:ordre)
  end

  # GET /travaux_statuts/1
  # GET /travaux_statuts/1.json
  def show
  end

  # GET /travaux_statuts/new
  def new
    @travaux_statut = TravauxStatut.new
  end

  # GET /travaux_statuts/1/edit
  def edit
  end

  # POST /travaux_statuts
  # POST /travaux_statuts.json
  def create
    @travaux_statut = TravauxStatut.new(travaux_statut_params)

    respond_to do |format|
      if @travaux_statut.save
        format.html { redirect_to @travaux_statut, notice: 'Travaux statut was successfully created.' }
        format.json { render :show, status: :created, location: @travaux_statut }
      else
        format.html { render :new }
        format.json { render json: @travaux_statut.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /travaux_statuts/1
  # PATCH/PUT /travaux_statuts/1.json
  def update
    respond_to do |format|
      if @travaux_statut.update(travaux_statut_params)
        format.html { redirect_to @travaux_statut, notice: 'Travaux statut was successfully updated.' }
        format.json { render :show, status: :ok, location: @travaux_statut }
      else
        format.html { render :edit }
        format.json { render json: @travaux_statut.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travaux_statuts/1
  # DELETE /travaux_statuts/1.json
  def destroy
    @travaux_statut.destroy
    respond_to do |format|
      format.html { redirect_to travaux_statuts_url, notice: 'Travaux statut was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travaux_statut
      @travaux_statut = TravauxStatut.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travaux_statut_params
      params.require(:travaux_statut).permit(:nom, :ordre)
    end
end
