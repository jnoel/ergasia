class TravauxArtisansController < ApplicationController
  include ApplicationHelper
  before_action :set_travaux_artisan, only: [:show, :edit, :update, :destroy]

  # GET /travaux_artisans
  # GET /travaux_artisans.json
  def index
    @travaux_artisans = TravauxArtisan.all
  end

  # GET /travaux_artisans/1
  # GET /travaux_artisans/1.json
  def show
  end

  # GET /travaux_artisans/new
  def new
    @travaux_artisan = TravauxArtisan.new
  end

  # GET /travaux_artisans/1/edit
  def edit
  end

  # POST /travaux_artisans
  # POST /travaux_artisans.json
  def create
    @travaux_artisan = TravauxArtisan.new(travaux_artisan_params)

    respond_to do |format|
      if @travaux_artisan.save
        ligne = TravauxLigne.create(travail_id: @travaux_artisan.travail_id,
            												detail: "Ajout de #{@travaux_artisan.tier.nom_complet} a la liste des artisans pour ce travail",
                                    message_automatique: true,
            												user_id: current_user.id)
        format.html { redirect_to travail_path(@travaux_artisan.travail_id), notice: 'Travaux artisan was successfully created.' }
        format.json { render :show, status: :created, location: @travaux_artisan }
      else
        format.html { render :new }
        format.json { render json: @travaux_artisan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /travaux_artisans/1
  # PATCH/PUT /travaux_artisans/1.json
  def update
    respond_to do |format|
      demande = @travaux_artisan.demande ? 1 : 0
      bailleur = @travaux_artisan.bailleur ? 1 : 0
      fin_travaux = @travaux_artisan.fin_travaux ? 1 : 0
      paye = @travaux_artisan.paye ? 1 : 0
      bon_pour_paiement = @travaux_artisan.bon_pour_paiement ? 1 : 0
      accord_check = @travaux_artisan.accord_check ? 1 : 0
      result = false
      cc = params["cc"]

    	message = nil
      pj = nil
      type_mail = nil
      if demande!=travaux_artisan_params["demande"].to_i
        @travaux_artisan.travail.update(travaux_statut_id: 12) if @travaux_artisan.travail.travaux_statut.ordre < 1.5
    		message = ""
        mail = params["mail"]
        if mail.to_s.empty?
          message = "Demande travaux envoyée manuellement à #{@travaux_artisan.tier.nom_complet}"
        else
          message = "Demande travaux envoyée par courriel à #{@travaux_artisan.tier.nom_complet} - #{@travaux_artisan.tier.email}\n\n#{cc ? 'cc: '+cc : ''}\n\n#{mail}"
        end
        pdf = TravauxArtisansHelper::FicheIntervention.new(@travaux_artisan.id, travaux_artisan_params["fi_devis"]=="1", travaux_artisan_params["fi_details"], travaux_artisan_params["fi_montant"])
        file = StringIO.new(pdf.render)
        file.class.class_eval { attr_accessor :original_filename, :content_type } #add attr's that paperclip needs
        file.original_filename = "fiche_intervention_travaux_#{@travaux_artisan.id}.pdf"
        file.content_type = "application/pdf"
        pj = file
        type_mail = "demande_intervention" unless mail.to_s.empty?
    	end
      if travaux_artisan_params["devis"]
    		@travaux_artisan.travail.update(travaux_statut_id: 2) if @travaux_artisan.travail.travaux_statut.ordre < 2.0
				message = "Réception du devis de #{@travaux_artisan.tier.nom_complet}"
        pj = travaux_artisan_params["devis"]
    	end
      if bailleur!=travaux_artisan_params["bailleur"].to_i
        @travaux_artisan.travail.update(travaux_statut_id: 13) if @travaux_artisan.travail.travaux_statut.ordre < 2.2
        pj = @travaux_artisan.devis
        mail = params["mail"]
        message = "Devis de #{@travaux_artisan.tier.nom_complet} envoyé#{mail.to_s.empty? ? " manuellement" : ""} au bailleur"
        message += "\n\n#{mail}" unless mail.to_s.empty?
        type_mail = "devis_bailleur" unless mail.to_s.empty?
    	end
      if params["retour_bailleur"]
        @travaux_artisan.travail.update(travaux_statut_id: 14) if @travaux_artisan.travail.travaux_statut.ordre < 2.5
        if travaux_artisan_params["accord_bailleur"]=="1"
          message = "Réception du bon pour accord du bailleur pour le devis de #{@travaux_artisan.tier.nom_complet}\n\n#{travaux_artisan_params["accord_bailleur_message"]}"
        else
          message = "Le bailleur refuse le devis de #{@travaux_artisan.tier.nom_complet}\n\n#{travaux_artisan_params["accord_bailleur_message"]}"
        end
        pj = travaux_artisan_params["accord_bailleur_fichier"]
      end
    	if accord_check!=travaux_artisan_params["accord_check"].to_i
    		@travaux_artisan.travail.update(travaux_statut_id: 3) if @travaux_artisan.travail.travaux_statut.ordre < 3.0
				message = "Envoi#{mail.to_s.empty? ? " manuel" : ""} du bon pour accord du devis à l'artisan #{@travaux_artisan.tier.nom_complet}"
        `pdftk "#{Rails.root.join('public', @travaux_artisan.devis.path)}" multistamp #{Rails.root.join('tmp', 'pdf', 'bpa.pdf')} output "#{Rails.root.join('tmp', 'pdf', @travaux_artisan.devis_file_name)}"`
        pj = File.open(Rails.root.join('tmp', 'pdf', @travaux_artisan.devis_file_name))
        mail = params["mail"]
        message += "\n\n#{mail}" unless mail.to_s.empty?
        type_mail = "bpa" unless mail.to_s.empty?
    	end
    	if travaux_artisan_params["facture"]
    		@travaux_artisan.travail.update(travaux_statut_id: 5) if @travaux_artisan.travail.travaux_statut.ordre < 5.0
				message = "Réception de la facture de #{@travaux_artisan.tier.nom_complet}"
        pj = travaux_artisan_params["facture"]
    	end
    	if travaux_artisan_params["fin_travaux"]
    		@travaux_artisan.travail.update(travaux_statut_id: 4) if @travaux_artisan.travail.travaux_statut.ordre < 4.0
    		message = "#{@travaux_artisan.tier.nom_complet} a fini les travaux"
        pj = travaux_artisan_params["fin_travaux"]
    	end
    	if paye!=travaux_artisan_params["paye"].to_i
    		@travaux_artisan.travail.update(travaux_statut_id: 6) if @travaux_artisan.travail.travaux_statut.ordre < 6.0
    		message = "Rôle Gercop: paiement de la facture de #{@travaux_artisan.tier.nom_complet}"
    	end
      if bon_pour_paiement!=travaux_artisan_params["bon_pour_paiement"].to_i
        @travaux_artisan.travail.update(travaux_statut_id: 15) if @travaux_artisan.travail.travaux_statut.ordre < 5.5
    		message = "Bon pour paiement de la facture de #{@travaux_artisan.tier.nom_complet}\nType de bpp : #{@travaux_artisan.type_paiement}"
    	end
    	if message
	    	ligne = TravauxLigne.create(travail_id: @travaux_artisan.travail_id,
																		detail: message,
																		message_automatique: true,
                                    piece_jointe: pj,
																		user_id: current_user.id)
        if ligne
          result = @travaux_artisan.update(travaux_artisan_params)
          if accord_check!=travaux_artisan_params["accord_check"].to_i
            @travaux_artisan.accord = pj
            @travaux_artisan.save
          end
          if type_mail
            to = nil
            sujet = nil
            case type_mail
            when "demande_intervention"
              to = @travaux_artisan.tier.email
              sujet = "Demande d'intervention"
            when "devis_bailleur"
              to = @travaux_artisan.travail.bien.proprietaire.email
              sujet = "Devis pour travaux"
            when "bpa"
              to = @travaux_artisan.tier.email
              sujet = "Bon pour accord"
            end
            TravauxArtisanMailer.mail_travaux_artisan(current_user.email, to, mail, ligne, sujet, cc).deliver_now
          end
        end
	  	else
        result = @travaux_artisan.update(travaux_artisan_params)
      end
      if result
        format.html { redirect_to :back, notice: 'Travaux artisan was successfully updated.' }
        format.json { render :show, status: :ok, location: @travaux_artisan }
      else
        format.html { render :edit }
        format.json { render json: @travaux_artisan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travaux_artisans/1
  # DELETE /travaux_artisans/1.json
  def destroy
    ligne = TravauxLigne.create(travail_id: @travaux_artisan.travail_id,
        												detail: "#{@travaux_artisan.tier.nom_complet} a été retiré de la liste des artisans pour ce travail",
                                message_automatique: true,
        												user_id: current_user.id)
    @travaux_artisan.destroy
    respond_to do |format|
      format.html { redirect_to travail_path(@travaux_artisan.travail_id), notice: 'Travaux artisan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travaux_artisan
      @travaux_artisan = TravauxArtisan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travaux_artisan_params
      params.require(:travaux_artisan).permit(:travail_id, :tier_id, :devis, :bailleur, :accord, :accord_check, :facture, :demande, :paye, :fin_travaux, :bon_pour_paiement, :type_paiement, :fi_devis, :fi_details, :fi_montant, :accord_bailleur, :accord_bailleur_message, :accord_bailleur_fichier)
    end
end
