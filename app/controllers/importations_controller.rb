class ImportationsController < ApplicationController
  include ImportationsHelper
  before_action :set_importation, only: [:show, :edit, :update, :destroy]

  # GET /importations
  # GET /importations.json
  def index
    @importations = Importation.all
  end

  # GET /importations/1
  # GET /importations/1.json
  def show
  end

  # GET /importations/new
  def new
    @importation = Importation.new
  end

  # GET /importations/1/edit
  def edit
  end

  # POST /importations
  # POST /importations.json
  def create
    @importation = Importation.new(importation_params)
    @importation.user_id = current_user.id
    respond_to do |format|
      if @importation.save
        import_tiers @importation if @importation.type=="Tiers"
        import_biens @importation if @importation.type=="Biens"
        format.html { redirect_to @importation, notice: 'Importation was successfully created.' }
        format.json { render :show, status: :created, location: @importation }
      else
        format.html { render :new }
        format.json { render json: @importation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /importations/1
  # PATCH/PUT /importations/1.json
  def update
    respond_to do |format|
      if @importation.update(importation_params)
        format.html { redirect_to @importation, notice: 'Importation was successfully updated.' }
        format.json { render :show, status: :ok, location: @importation }
      else
        format.html { render :edit }
        format.json { render json: @importation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /importations/1
  # DELETE /importations/1.json
  def destroy
    @importation.destroy
    respond_to do |format|
      format.html { redirect_to importations_url, notice: 'Importation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_importation
      @importation = Importation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def importation_params
      params.require(:importation).permit(:user_id, :titre, :notes, :fichier, :type)
    end
end
