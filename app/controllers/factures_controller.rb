class FacturesController < ApplicationController

  skip_before_action :verify_authenticity_token

  # GET factures
  def index
    travaux_artisans = TravauxArtisan.where("facture_file_size>0")
    @titre = "Factures"
    @sous_menu = false
    @sous_menu = nil
    if params["bpp"]=="1"
      travaux_artisans = travaux_artisans.where(bon_pour_paiement: true, paye: false)
      @titre += " à payer"
      @sous_menu = "bpp"
    elsif params["artisan"]
      travaux_artisans = travaux_artisans.where(tier_id: params["artisan"])
      @titre += " - #{travaux_artisans[0].tier.nom_complet}" if travaux_artisans[0]
    elsif params["send"]=="1"
      @sous_menu = "send"
      travaux_artisans = travaux_artisans.where(paye: true, facture_envoye: false)
      @titre += " à envoyer"
    end
    if params["type_paiement"]=="DG"
      travaux_artisans = travaux_artisans.where(type_paiement: "DG")
      @titre += "\n(dépôt de garantie)"
    elsif params["type_paiement"]=="LO"
      travaux_artisans = travaux_artisans.where(type_paiement: "LO")
      @titre += " \n(Loyer)"
    elsif params["type_paiement"]=="PR"
      travaux_artisans = travaux_artisans.where(type_paiement: "PR")
      @titre += " \n(Propriétaire)"
    elsif params["type_paiement"]=="AS"
      travaux_artisans = travaux_artisans.where(type_paiement: "AS")
      @titre += " \n(Assurance)"
    end
    gon.factures = []
  	travaux_artisans.each do |ta|
      email_facture = "Bonjour\n\nVeuillez trouver ci-joint la facture d'intervention retenue sur le loyer de ce mois pour le logement situé:"
  		email_facture += "\n\nBien : #{ta.travail.bien.nom}\nAdresse : #{ta.travail.bien.adresse_complete}"
    	email_facture += "\n\nCordialement,"
    	email_facture += "\n\n#{current_user.signature}" if current_user.signature
  		gon.factures << {id: ta.id,
    									date: (ta.facture_updated_at ? I18n.l(ta.facture_updated_at.in_time_zone(current_user.timezone), format: :ultra_short_datetime) : ""),
    									artisan: ta.tier.nom_complet,
                      artisan_id: ta.tier.id,
                      bien: ta.travail.bien.nom,
                      bien_id: ta.travail.bien.id,
    									proprietaire: ta.travail.bien.proprietaire.nom_complet,
                      proprietaire_email: ta.travail.bien.proprietaire.email,
                      proprietaire_id: ta.travail.bien.proprietaire.id,
    									locataire: ta.travail.bien.locataire ? ta.travail.bien.locataire.nom_complet : "",
                      locataire_id: ta.travail.bien.locataire ? ta.travail.bien.locataire.id : "",
    									travaux: ta.travail.travaux_type.nom,
                      travaux_id: ta.travail.id,
    									date_bpp: I18n.l(ta.updated_at.in_time_zone(current_user.timezone), format: :ultra_short_datetime),
                      url: ta.facture.url,
                      a_payer: (ta.bon_pour_paiement and !ta.paye),
                      a_envoye: (ta.paye and !ta.facture_envoye),
                      email_facture: email_facture,
                      type_paiement: ta.type_paiement
    								 }
  	end
  end

  # POST factures
  def paye
    ta = TravauxArtisan.find(params["id"])
    ta.paye = true

    respond_to do |format|
      if ta.save
    		ta.travail.update(travaux_statut_id: 6)
    		message = "Rôle Gercop: paiement de la facture de #{ta.tier.nom_complet}"
  	    ligne = TravauxLigne.create(travail_id: ta.travail_id,
																		detail: message,
																		message_automatique: true,
                                    piece_jointe: nil,
																		user_id: current_user.id)
        format.html { redirect_to "/factures", notice: 'Facture payée' }
      else
        format.html { redirect_to "/factures", notice: 'Impossible de marquer la facture comme payée' }
      end
    end

  end

  # POST factures
  def envoye
    ta = TravauxArtisan.find(params["emailFactureModalId"])
    ta.facture_envoye = true
    respond_to do |format|
      if ta.save
        sujet = 'Facture travaux'
        corps = params["emailFactureModalMessage"]
        to = params["emailFactureModalTo"]
    		message = "Rôle Gercop: facture envoyée à #{ta.travail.bien.proprietaire.nom_complet}"
        message += "\n\n#{corps}" if params["emailFactureModalCheckMail"]=="1"
  	    ligne = TravauxLigne.create(travail_id: ta.travail_id,
																		detail: message,
																		message_automatique: true,
                                    piece_jointe: ta.facture,
																		user_id: current_user.id)
        TravauxArtisanMailer.mail_travaux_artisan(current_user.email, to, corps, ligne, sujet, nil).deliver_now if params["emailFactureModalCheckMail"]=="1"
        format.html { redirect_to "/factures", notice: 'Facture envoyée' }
      else
        format.html { redirect_to "/factures", notice: 'Impossible de marquer la facture comme envoyée' }
      end
    end
  end

end
