class TravauxController < ApplicationController
	include ApplicationHelper
  before_action :set_travail, only: [:show, :edit, :update, :destroy]

  # GET /travaux
  # GET /travaux.json
  def index
  	tiers_id = params["tiers"]
  	bien_id = params["bien"]
  	@titre = "TRAVAUX"
  	if params["artisan"]
			@travauxartisans = TravauxArtisan.includes(:travail, :tier).where(tier_id: params["artisan"])
			artisan = Tier.find(params["artisan"])
			@titre += " - #{artisan.nom_complet}"
			gon.travaux = []
	  	@travauxartisans.each do |ta|
	  		artisans = TravauxArtisan.where(travail_id: ta.travail_id)
	  		artisans_tab = []
	  		artisans.each do |artisan|
	  			artisans_tab << artisan.tier.nom_complet unless artisans_tab.include?(artisan.tier.nom_complet)
	  		end
				proprietaires = []
	      proprietaires << ta.travail.bien.proprietaire.nom_complet if ta.travail.bien.proprietaire
	      proprietaires << ta.travail.bien.coproprietaire.nom_complet if ta.travail.bien.coproprietaire
	      locataires = []
	      locataires << ta.travail.bien.locataire.nom_complet if ta.travail.bien.locataire
	      locataires << ta.travail.bien.colocataire.nom_complet if ta.travail.bien.colocataire
	  		gon.travaux << {id: ta.travail_id,
	  										type: ta.travail.travaux_type.nom,
												statut: ta.travail.travaux_statut.nom,
												date: ta.travail.created_at.strftime("%d/%m/%Y"),
												bien: ta.travail.bien.nom,
												proprietaire: proprietaires.join(", "),
												locataire: locataires.join(", "),
												prestataires: artisans_tab.join(", ")
											 }
			end
  	else
			@travaux = Travail.all
			if tiers_id
		  	@travaux = @travaux.joins(:bien).where("biens.proprietaire_id=? OR biens.locataire_id=?", tiers_id, tiers_id)
		  	tier = Tier.find(tiers_id)
				@titre += " - #{tier.nom_complet}"
		  end
		  if bien_id
		  	@travaux = @travaux.where(:bien_id => bien_id)
		  	bien = Bien.find(bien_id)
				@titre += " - #{bien.nom}"
		  end

	  	gon.travaux = []
	  	@travaux.each do |travail|
	  		artisans = TravauxArtisan.includes(:tier).where(travail_id: travail.id)
	  		artisans_tab = []
	  		artisans.each do |artisan|
	  			artisans_tab << artisan.tier.nom_complet unless artisans_tab.include?(artisan.tier.nom_complet)
	  		end
				proprietaires = []
	      proprietaires << travail.bien.proprietaire.nom_complet if travail.bien.proprietaire
	      proprietaires << travail.bien.coproprietaire.nom_complet if travail.bien.coproprietaire
	      locataires = []
	      locataires << travail.bien.locataire.nom_complet if travail.bien.locataire
	      locataires << travail.bien.colocataire.nom_complet if travail.bien.colocataire
	  		gon.travaux << {id: travail.id,
	  										type: travail.travaux_type.nom,
												statut: travail.cancel ? "Annulé" : travail.travaux_statut.nom,
												date: travail.created_at.strftime("%d/%m/%Y"),
												bien: travail.bien.nom,
												proprietaire: proprietaires.join(", "),
												locataire: locataires.join(", "),
												prestataires: artisans_tab.join(", ")
											 }
		  end
		end
    @new_path = new_travail_path
    @new_path += "?bien=#{bien_id}" if bien_id
		@new_path += "?tiers=#{tiers_id}" if tiers_id
  end

  # GET /travaux/1
  # GET /travaux/1.json
  def show
  	@lignes = TravauxLigne.where(travail_id: @travail.id)
  	@artisans = Tier.where(tiers_type_id: 3).order(:raison_sociale, :nom, :prenom)
  	@prestataires = TravauxArtisan.where(travail_id: @travail.id).order(:id)
  	@superviseurs = Superviseur.where(travail_id: @travail.id)
  	@email = "Bonjour\n\nNous avons besoin de faire appel à vos services:\nNature des travaux : #{@travail.travaux_type.nom}"
  	@email += "\n\nLogement concerné :\n\nBien : #{@travail.bien.nom}\nAdresse : #{@travail.bien.adresse_complete}\nPropriétaire : #{@travail.bien.proprietaire.nom_complet}"
  	@email += "\nLocataire : #{@travail.bien.locataire.nom_complet}" if @travail.bien.locataire
  	@email += "\n\nVous remerciant de votre retour.\nCordialement,"
  	@email += "\n\n#{current_user.signature}" if current_user.signature
		@email_bailleur = "Bonjour\n\nJe vous transmets en pièce jointe pour avis le devis pour les travaux à réaliser à l'adresse suivante :"
  	@email_bailleur += "\n\nBien : #{@travail.bien.nom}\nAdresse : #{@travail.bien.adresse_complete}"
  	@email_bailleur += "\n\nAu plaisir de recevoir votre retour.\nVous remerciant de votre confiance.\n\nCordialement,"
  	@email_bailleur += "\n\n#{current_user.signature}" if current_user.signature
		@email_accord = "Bonjour\n\nJe vous transmets le bon pour accord d'intervention pour le logement situé:"
		@email_accord += "\n\nBien : #{@travail.bien.nom}\nAdresse : #{@travail.bien.adresse_complete}"
  	@email_accord += "\n\nCordialement,"
  	@email_accord += "\n\n#{current_user.signature}" if current_user.signature
  	Messagerie.joins(:travaux_ligne).where("travaux_lignes.travail_id": @travail.id, user_id: current_user.id).destroy_all
		@travaux_ligne = TravauxLigne.new
		@travaux_ligne.travail_id = @travail.id
		@supprimable = TravauxArtisan.where(travail_id: @travail.id, demande: true).count == 0
  end

  # GET /travaux/new
  def new
    @travail = Travail.new
    @travail.bien_id = params["bien"] if params["bien"]
		tiers_id = params["tiers"]
		@biens = tiers_id ? Bien.where("proprietaire_id=? OR locataire_id=?", tiers_id, tiers_id) : Bien.all
  end

  # GET /travaux/1/edit
  def edit
  end

  # POST /travaux
  # POST /travaux.json
  def create
    @travail = Travail.new(travail_params)
    @travail.user_id = current_user.id
    @travail.travaux_statut_id = 1
    respond_to do |format|
      if @travail.save
				# Ajouter les superviseurs généraux
				User.all.each do |user|
					Superviseur.create :travail_id => @travail.id, :user_id => user.id if user.superviseur
        end
				format.html { redirect_to @travail, notice: 'Travail was successfully created.' }
        format.json { render :show, status: :created, location: @travail }
      else
        format.html { render :new }
        format.json { render json: @travail.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /newligne
  def newligne
  	@ligne = TravauxLigne.new(travaux_ligne_params, user_id: current_user.id)
  	respond_to do |format|
      if @ligne.save
        format.html { redirect_to "/travaux/#{params['idtravail']}#nouvelleligne", notice: 'Ligne was successfully created.' }
      end
    end
  end

  # PATCH/PUT /travaux/1
  # PATCH/PUT /travaux/1.json
  def update
    if travail_params["cancel"]=="1"
			TravauxLigne.create(travail_id: @travail.id,
													detail: "Annulation du travail :\n#{travail_params["cancel_raison"]}",
													message_automatique: true,
													user_id: current_user.id)
		end
		respond_to do |format|
      if @travail.update(travail_params)
        format.html { redirect_to @travail, notice: 'Travail was successfully updated.' }
        format.json { render :show, status: :ok, location: @travail }
      else
        format.html { render :edit }
        format.json { render json: @travail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travaux/1
  # DELETE /travaux/1.json
  def destroy
    @travail.destroy
    respond_to do |format|
      format.html { redirect_to travaux_url, notice: 'Travail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travail
      @travail = Travail.includes(:travaux_type, :travaux_statut, :bien).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travail_params
      params.require(:travail).permit(:bien_id, :travaux_statut_id, :travaux_type_id, :cancel, :cancel_raison)
    end

		# Never trust parameters from the scary internet, only allow the white list through.
    def travaux_ligne_params
      params.require(:travaux_ligne).permit(:travail_id, :detail, :piece_jointe)
    end
end
