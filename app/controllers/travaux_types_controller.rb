class TravauxTypesController < ApplicationController
  before_action :set_travaux_type, only: [:show, :edit, :update, :destroy]

  # GET /travaux_types
  # GET /travaux_types.json
  def index
    @travaux_types = TravauxType.all
  end

  # GET /travaux_types/1
  # GET /travaux_types/1.json
  def show
  end

  # GET /travaux_types/new
  def new
    @travaux_type = TravauxType.new
  end

  # GET /travaux_types/1/edit
  def edit
  end

  # POST /travaux_types
  # POST /travaux_types.json
  def create
    @travaux_type = TravauxType.new(travaux_type_params)

    respond_to do |format|
      if @travaux_type.save
        format.html { redirect_to @travaux_type, notice: 'Travaux type was successfully created.' }
        format.json { render :show, status: :created, location: @travaux_type }
      else
        format.html { render :new }
        format.json { render json: @travaux_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /travaux_types/1
  # PATCH/PUT /travaux_types/1.json
  def update
    respond_to do |format|
      if @travaux_type.update(travaux_type_params)
        format.html { redirect_to @travaux_type, notice: 'Travaux type was successfully updated.' }
        format.json { render :show, status: :ok, location: @travaux_type }
      else
        format.html { render :edit }
        format.json { render json: @travaux_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travaux_types/1
  # DELETE /travaux_types/1.json
  def destroy
    @travaux_type.destroy
    respond_to do |format|
      format.html { redirect_to travaux_types_url, notice: 'Travaux type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travaux_type
      @travaux_type = TravauxType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travaux_type_params
      params.require(:travaux_type).permit(:nom)
    end
end
