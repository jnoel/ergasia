$ ->
  $('#emailFactureModal').on('show.bs.modal', (event) ->
    button = $(event.relatedTarget)
    modal = $(this)
    to = button.data('email')
    $('#emailFactureModalTo').val(to)
    id = button.data('id')
    message = button.data('message')
    $('#emailFactureModalMessage').val(message)
    $('#emailFactureModalId').val(id)
  )

  $('#emailFactureModalCheckMail').on "click", ->
  	if $(this).is ':checked'
  		$('#emailFactureModalHidden').removeClass("hidden")
  		$('#emailFactureModalMailButton').text("Envoyer la facture par mail")
  		$('#emailFactureModalMailButton').removeClass("btn-default").addClass("btn-primary")
  	else
  		$('#emailFactureModalHidden').addClass("hidden")
  		$('#emailFactureModalMailButton').text("Envoyer la facture manuellement")
  		$('#emailFactureModalMailButton').removeClass("btn-primary").addClass("btn-default")
