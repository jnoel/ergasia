# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
	$('#demandeModal').on('show.bs.modal', (event) ->
		button = $(event.relatedTarget)
		modal = $(this)
		to = button.data('email')
		$('#demandeModalTo').val(to)
		id = button.data('id')
		if button.data('email-proprietaire')
			$('#demandeModalCcProprietaire').trigger('click')
		else
			$('#demandeModalCcProprietaire').prop('disabled', true)
			$('#demandeModalCcProprietaireLabel').text("Le propr. n'a pas d'email")
		if button.data('email-locataire')
			$('#demandeModalCcLocataire').trigger('click')
		else
			$('#demandeModalCcLocataire').prop('disabled', true)
			$('#demandeModalCcLocataireLabel').text("Le loc. n'a pas d'email")
		$('#demandeModalMailButton').on "click", ->
			$('#glyph_demande_'+id).removeClass("fa-times-circle check red").addClass("fa-cog fa-spin green")
			message = $('#demandeModalMessage').val()
			to = $('#demandeModalTo').val()
			cc = ""
			if $('#demandeModalCcProprietaire').is ':checked'
				cc += button.data('email-proprietaire')
			if $('#demandeModalCcLocataire').is ':checked'
				if cc
					cc += ","
				cc += button.data('email-locataire')
			if $('#demandeModalCcUser').is ':checked'
				if cc
					cc += ","
				cc += button.data('email-user')
			fi_details = $('#demandeModalFiDetails').val()
			fi_montant = $('#demandeModalMontant').val()
			if $('#demandeModalCheckMail').is ':checked'
				$("#mail"+id).val(message)
				$("#cc"+id).val(cc)
			$("#fi_details"+id).val(fi_details)
			$("#fi_montant"+id).val(fi_montant)
			if $('#demandeModalFiDevis').is ':checked'
	  		$("#fi_devis"+id).trigger('click')
			$('#demande'+id).trigger('click')
	)

	$('#demandeModalCheckMail').on "click", ->
		if $(this).is ':checked'
			$('#demandeModalHidden').removeClass("hidden")
			$('#demandeModalMailButton').text("Envoyer la demande par mail")
			$('#demandeModalMailButton').removeClass("btn-default").addClass("btn-primary")
		else
			$('#demandeModalHidden').addClass("hidden")
			$('#demandeModalMailButton').text("Envoyer la demande manuellement")
			$('#demandeModalMailButton').removeClass("btn-primary").addClass("btn-default")

	$('#demandeModalFiDevis').on "click", ->
		if $(this).is ':checked'
			$('#demandeModalMontantHidden').removeClass("hidden")
		else
			$('#demandeModalMontantHidden').addClass("hidden")

	$('#bailleurModal').on('show.bs.modal', (event) ->
		button = $(event.relatedTarget)
		modal = $(this)
		to = button.data('email')
		$('#bailleurModalTo').val(to)
		id = button.data('id')
		$('#bailleurModalMailButton').on "click", ->
			$('#glyph_bailleur_'+id).removeClass("fa-times-circle check red").addClass("fa-cog fa-spin green")
			message = $('#bailleurModalMessage').val()
			if $('#bailleurModalCheckMail').is ':checked'
				$("#mail"+id).val(message)
			$('#bailleur'+id).trigger('click')
	)

	$('#bailleurModalCheckMail').on "click", ->
		if $(this).is ':checked'
			$('#bailleurModalHidden').removeClass("hidden")
			$('#bailleurModalMailButton').text("Envoyer le devis par mail")
			$('#bailleurModalMailButton').removeClass("btn-default").addClass("btn-primary")
		else
			$('#bailleurModalHidden').addClass("hidden")
			$('#bailleurModalMailButton').text("Envoyer le devis manuellement")
			$('#bailleurModalMailButton').removeClass("btn-primary").addClass("btn-default")

	$('#accordModal').on('show.bs.modal', (event) ->
		button = $(event.relatedTarget)
		modal = $(this)
		to = button.data('email')
		$('#accordModalTo').val(to)
		id = button.data('id')
		$('#accordModalMailButton').on "click", ->
			$('#glyph_accord_'+id).removeClass("fa-times-circle check red").addClass("fa-cog fa-spin green")
			message = $('#accordModalMessage').val()
			if $('#accordModalCheckMail').is ':checked'
				$("#mail"+id).val(message)
			$('#accord_check'+id).trigger('click')
	)

	$('#accordModalCheckMail').on "click", ->
		if $(this).is ':checked'
			$('#accordModalHidden').removeClass("hidden")
			$('#accordModalMailButton').text("Envoyer le bon pour accord automatiquement signé par mail")
			$('#accordModalMailButton').removeClass("btn-default").addClass("btn-primary")
		else
			$('#accordModalHidden').addClass("hidden")
			$('#accordModalMailButton').text("Envoyer le bon pour accord automatiquement signé manuellement")
			$('#accordModalMailButton').removeClass("btn-primary").addClass("btn-default")

	$('#retourBailleurModal').on('show.bs.modal', (event) ->
		button = $(event.relatedTarget)
		modal = $(this)
		id = button.data('id')
		$('#retourBailleurFichierModal').on "click", ->
			$('#accord_bailleur_fichier'+id).trigger('click')
		$('#retourBailleurModalButton').on "click", ->
			$('#glyph_retour_bailleur_'+id).removeClass("fa-times-circle check red").addClass("fa-cog fa-spin green")
			message = $('#retourBailleurMessageModal').val()
			$("#accord_bailleur_message"+id).val(message)
			if $('#retourBailleurBooleanModal').is ':checked'
				$("#accord_bailleur"+id).trigger('click')
			$('#retour_bailleur'+id).trigger('click')
	)

	$('#bppModal').on('show.bs.modal', (event) ->
		button = $(event.relatedTarget)
		modal = $(this)
		id = button.data('id')
		$('#bppModalValidate').on "click", ->
			$('#glyph_bon_pour_paiement_'+id).removeClass("fa-times-circle check red").addClass("fa-cog fa-spin green")
			type_paiement = $('#bppModalSelect').val()
			$('#type_paiement'+id).val(type_paiement)
			$('#bon_pour_paiement'+id).trigger('click');
	)

	$('#cancelModal').on('show.bs.modal', (event) ->
		$('#cancelModalButton').on "click", ->
			message = $('#raisonCancelModal').val()
			if !message
				alert("La raison de l'annulation est obligatoire")
			else
				$("#raisonCancel").val(message)
				$("#checkCancel").trigger('click')
				$("#validateCancel").trigger('click')
	)
