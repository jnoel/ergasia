module TravauxArtisansHelper
  class FicheIntervention < Prawn::Document

  	def initialize id, fi_devis=true, fi_details=nil, fi_montant=nil, fichier=nil

  		super(
  					:page_layout => :portrait,
  					:page_size => 'A4'
  			 	 )

      @societe = Societe.find(1)
      @travaux_artisan = TravauxArtisan.find(id)
      @superviseur = Superviseur.where(travail_id: @travaux_artisan.travail_id).first
      @fi_devis = fi_devis
      @fi_details = fi_details
      @fi_montant = fi_montant
  		en_tete
  		corps id
  		rendu fichier if fichier
  	end

  	def en_tete

  		image @societe.logo.path, :height => 100 if @societe.logo?
  		width = 140
  		bounding_box([bounds.right-width, bounds.top], :width => width) do
  		 text "<b><i>TRANSACTION\n\nVENTE - LOCATION\n\nGESTION</i></b>", size: 12, inline_format: true
   		end
   		padding = 5
   		rectangle [bounds.right-width-padding, bounds.top+padding*2], width+padding, 90
   		stroke

  		bounding_box([bounds.left, bounds.top-120], :width => bounds.right) do
  		 text "#{@societe.adresse} – #{@societe.cp} #{@societe.ville} – Tél. : #{@societe.tel}   Fax : #{@societe.fax}", size: 10, inline_format: true, align: :center
       if @superviseur
         text "<u>Rôle travaux:</u> #{@superviseur.user.name}   –   #{@superviseur.user.tel}   –   email: #{@superviseur.user.email}", size: 10, inline_format: true, align: :center
       end
   		end

   		self.line_width = 5
   		stroke_color 'ffd231'
   		y = bounds.top-124
   		stroke do
   			horizontal_line bounds.left, 50, :at => y
   			horizontal_line bounds.right-50, bounds.right, :at => y
   		end
  	end

  	def corps id
  		y = bounds.top-170
  		bounding_box([bounds.left, y], :width => bounds.right) do
  		 text "<b>FICHE D'INTERVENTION TRAVAUX - #{Date.today.strftime('%d/%m/%Y')}</b>", size: 18, inline_format: true, :align => :center
   		end
   		padding = 5
   		stroke_color '000000'
   		self.line_width = 1
   		padding = 5
   		rectangle [bounds.left, y+padding],  bounds.right, 22
   		stroke

   		y = bounds.top-210
  		bounding_box([bounds.left+padding, y], :width => bounds.right) do
  		 text "<u>ENTREPRISE :</u> #{@travaux_artisan.tier.nom_complet}", size: 12, inline_format: true
  		 text "#{@travaux_artisan.tier.portable}            #{@travaux_artisan.tier.tel}", size: 12
  		 text "#{@travaux_artisan.tier.email}", size: 12
   		end
   		rectangle [bounds.left, y+padding],  bounds.right, 50
   		stroke

      bien = @travaux_artisan.travail.bien
   		y = cursor-20
  		bounding_box([bounds.left+padding, y], :width => bounds.right) do
  		 text "<u>LOCATAIRE :</u> #{bien.locataire ? bien.locataire.nom_complet : ""}", size: 12, inline_format: true
  		 text "#{bien.locataire ? bien.locataire.portable : ""}            #{bien.locataire ? bien.locataire.tel : ""}", size: 12
  		 text "#{bien.locataire ? bien.locataire.email : ""}", size: 12
  		 text "\n<b><u>Adresse de l'intervention :</u></b>", size: 12, inline_format: true
       text "#{bien.adresse} #{bien.cp} #{bien.ville}", size: 12, inline_format: true
  		 text "\n<u>PROPRIETAIRE :</u> #{bien.proprietaire.nom_complet}", size: 12, inline_format: true
  		 text "<b><u>Facturation</u>                    <u>Chez ACCORD IMMOBILIER</u></b>", size: 12, inline_format: true
  		 text "\nJoindre à la facture la fiche d'intervention signée", size: 12, inline_format: true
   		end
   		rectangle [bounds.left, y+padding],  bounds.right, y-cursor+padding
   		stroke

   		y = cursor-20
  		bounding_box([bounds.left+padding, y], :width => bounds.right) do
  		 text "<b>DEMANDE D'INTERVENTION</b>", size: 18, inline_format: true, :align => :center
  		 text "\n<b><u>Devis avant travaux :</u>      #{@fi_devis ? "OUI #{@fi_montant.to_f>0.0 ? "SI MONTANT SUPERIEUR A #{@fi_montant} €" : ""}" : "NON"}</b>", size: 12, inline_format: true
  		 text "\n<b><u>Anomalies constatées / Travaux à réaliser : </u></b>\n#{@fi_details}", size: 12, inline_format: true
   		end
   		rectangle [bounds.left, y+padding],  bounds.right, y-cursor+padding
   		stroke

   		y = cursor-20
  		bounding_box([bounds.left+padding, y], :width => bounds.right) do
  		 text "<b>FICHE D'INTERVENTION</b>", size: 18, inline_format: true, :align => :center
  		 text "\n<b><u>Date d'intervention et délai d'exécution :</u></b>", size: 12, inline_format: true
  		 text "\n\n\n<b><u>Signature Locataire :\n\n\n\n</u></b>", size: 12, inline_format: true
   		end
   		rectangle [bounds.left, y+padding],  bounds.right, y-cursor+padding
   		stroke
  	end

  	def rendu fichier
  		render_file fichier
  	end

  end
end
