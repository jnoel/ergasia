require "csv"
module ImportationsHelper
  def import_tiers importation
    i = 0
    nb_import = 0
    nb_erreur = 0
  	message = ""

    CSV.foreach(importation.fichier.path, {:col_sep => "|"}) {|ligne|
  		if (i>0) # Pour éviter la ligne d'en-tête du CSV
        tier = Tier.where('lower(raison_sociale) = ? AND lower(nom) = ? AND lower(prenom) = ?', ligne[3].to_s.downcase, ligne[5].to_s.downcase, ligne[6].to_s.downcase).first
        if !tier
          if ligne[14] or ligne[12]
            tier = Tier.where("portable = ? OR tel = ?", ligne[14].to_s, ligne[12].to_s).first
          elsif ligne[17]
            tier = Tier.where(:email => ligne[17].to_s).first
          end
  			end
  			tier = Tier.new unless tier
  			tier.raison_sociale = ligne[3].to_s if ligne[3]
        tier.civilite = ligne[4].to_s if ligne[4]
        tier.nom = ligne[5].to_s if ligne[5]
        tier.prenom = ligne[6].to_s if ligne[6]
        tier.adresse = "#{ligne[7]}\n#{ligne[8]}"
        tier.cp = ligne[9].to_s if ligne[9]
        tier.ville = ligne[10].to_s if ligne[10]
        tier.tel = ligne[12].to_s if ligne[12]
        tier.tel_bureau = ligne[13].to_s if ligne[13]
        tier.portable = ligne[14].to_s if ligne[14]
        tier.email = ligne[17].to_s if ligne[17]
        tier.notes = ligne[22].to_s if ligne[22]
        tier.tiers_type_id = case ligne[2]
          when "ARTISAN"
            3
          when "ARTISAN ACCORD"
            3
          when "SYNDIC"
            4
          else
            break
        end
        if tier.save
          message += "#{i+1} => Tier #{tier.raison_sociale} OK\n"
          nb_import += 1
        else
          message += "#{i+1} => ERREUR : #{tier.raison_sociale} KO\n"
          nb_erreur += 1
        end
      end
      i += 1
  	}
    message += "\nImportation terminée : Réussis => #{nb_import}    Erreurs => #{nb_erreur}   Sur nb lignes => #{i}"
    importation.notes = message
    importation.save
  end

  def import_biens importation
    i = 0
    nb_import_proprietaires = 0
    nb_import_locataires = 0
    nb_import_biens = 0
    nb_erreurs = 0
  	message = ""

    proprietaire_type = TiersType.where(nom: "Propriétaire").first.id
    locataire_type = TiersType.where(nom: "Locataire").first.id

    begin
      CSV.foreach(importation.fichier.path, {:col_sep => "|"}) {|ligne|
    		if (i>0) # Pour éviter la ligne d'en-tête du CSV
          proprietaire = Tier.where('lower(nom) = ? AND lower(prenom) = ?', ligne[74].to_s.downcase, ligne[75].to_s.downcase).first
          if !proprietaire and !ligne[84].to_s.empty?
            proprietaire = Tier.where("portable = ?", ligne[84].to_s).first
          end
          if !proprietaire and !ligne[82].to_s.empty?
            proprietaire = Tier.where("tel = ?", ligne[82].to_s).first
          end
          if !proprietaire and !ligne[86].to_s.empty?
            proprietaire = Tier.where(:email => ligne[86].to_s).first
          end
          if !proprietaire and !ligne[72].to_s.empty?
            proprietaire = Tier.where('lower(nom) = ? AND lower(raison_sociale) = ?', ligne[74].to_s.downcase, ligne[72].to_s.downcase).first
    			end
    			proprietaire = Tier.new unless proprietaire
    			proprietaire.raison_sociale = ligne[72].to_s if ligne[72]
          proprietaire.civilite = ligne[73].to_s if ligne[73]
          proprietaire.nom = ligne[74].to_s if ligne[74]
          proprietaire.prenom = ligne[75].to_s if ligne[75]
          proprietaire.adresse = "#{ligne[77]}\n#{ligne[78]}"
          proprietaire.cp = ligne[79].to_s if ligne[79]
          proprietaire.ville = ligne[80].to_s if ligne[80]
          proprietaire.tel = ligne[82].to_s if ligne[82]
          proprietaire.tel_bureau = ligne[83].to_s if ligne[83]
          proprietaire.portable = ligne[84].to_s if ligne[84]
          proprietaire.fax = ligne[85].to_s if ligne[85]
          proprietaire.email = ligne[86].to_s if ligne[86]
          proprietaire.notes = ligne[95].to_s if ligne[95]
          proprietaire.tiers_type_id = proprietaire_type
          result_proprietaire = proprietaire.save
          if result_proprietaire
            message += "#{i+1} => Proprietaire #{proprietaire.nom_complet} OK\n"
            nb_import_proprietaires += 1
          else
            message += "#{i+1} => ERREUR : #{proprietaire.nom_complet} KO\n"
            nb_erreurs += 1
          end

          locataire = Tier.where('lower(nom) = ? AND lower(prenom) = ?', ligne[122].to_s.downcase, ligne[123].to_s.downcase).first
          if !locataire and !ligne[132].to_s.empty?
            locataire = Tier.where("portable = ?", ligne[132].to_s).first
          end
          if !locataire and !ligne[130].to_s.empty?
            locataire = Tier.where("tel = ?", ligne[130].to_s).first
          end
          if !locataire and !ligne[134].to_s.empty?
            locataire = Tier.where(:email => ligne[134].to_s).first
          end
          if !locataire and !ligne[120].to_s.empty?
            locataire = Tier.where('lower(nom) = ? AND lower(raison_sociale) = ?', ligne[122].to_s.downcase, ligne[120].to_s.downcase).first
    			end

    			locataire = Tier.new unless locataire
    			locataire.raison_sociale = ligne[120].to_s if ligne[120]
          locataire.civilite = ligne[121].to_s if ligne[121]
          locataire.nom = ligne[122].to_s if ligne[122]
          locataire.prenom = ligne[123].to_s if ligne[123]
          locataire.adresse = "#{ligne[77]}\n#{ligne[78]}"
          locataire.cp = ligne[79].to_s if ligne[79]
          locataire.ville = ligne[80].to_s if ligne[80]
          locataire.tel = ligne[130].to_s if ligne[130]
          locataire.tel_bureau = ligne[131].to_s if ligne[131]
          locataire.portable = ligne[132].to_s if ligne[132]
          locataire.fax = ligne[133].to_s if ligne[133]
          locataire.email = ligne[134].to_s if ligne[134]
          locataire.notes = ligne[135].to_s if ligne[135]
          locataire.tiers_type_id = locataire_type
          result_locataire = locataire.save
          if result_locataire
            message += "#{i+1} => Locataire #{locataire.nom_complet} OK\n"
            nb_import_locataires += 1
          else
            message += "#{i+1} => ERREUR : #{locataire.nom_complet} KO\n"
            nb_erreurs += 1
          end

          if result_locataire and result_proprietaire
            bien = Bien.where(ref: ligne[38].to_s).first
            bien = Bien.new unless bien
            bien.ref = ligne[38].to_s if ligne[38]
            bien.adresse = "#{ligne[52]} #{ligne[53]} #{ligne[54]}\n#{ligne[55]}"
            bien.cp = ligne[56] if ligne[56]
            bien.ville = ligne[57] if ligne[57]
            bien.bien_type_id = biens_type_depuis_pericles ligne[41].to_i
            bien.proprietaire_id = proprietaire.id
            bien.locataire_id = locataire.id
            if bien.save
              message += "#{i+1} => Bien #{bien.nom_ref} OK\n"
              nb_import_biens += 1
            else
              message += "#{i+1} => ERREUR : #{bien.nom_ref} KO\n"
              nb_erreurs += 1
            end
          end
        end
        i += 1
    	}
      message += "\nImportation terminée : Propriétaires => #{nb_import_proprietaires}    Locataires => #{nb_import_locataires}      Biens => #{nb_import_biens}     Erreurs => #{nb_erreurs}   Sur nb lignes => #{i}"
    rescue
      message += "\nArrêt imprévu de l'importation à la ligne #{i+1}"
    end
    importation.notes = message
    importation.save
  end

  def biens_type_depuis_pericles type_pericles
  	type = ""
  	case type_pericles
  		when 1,11
  			type = "Appartement"
  		when 2,12
  			type = "Villa"
  		when 3
  			type = "Terrain"
  		when 4
  			type = "Immeuble"
  		when 5
  			type = "Locaux"
  		when 6
  			type = "Fond de commerce"
  		when 7,14
  			type = "Parking"
  		when 13
  			type = "Bureaux"
  	end
    bien_type = BienType.where(nom: type).first
    unless bien_type
      bien_type = BienType.new
      bien_type.nom = type
      bien_type.save
    end
  	return bien_type.id
  end
end
